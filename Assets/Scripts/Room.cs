﻿using UnityEngine;
using System.Collections;

public class Room : MonoBehaviour
{
	public Transform[] playerPositions;

	public ItemPoint[] itemPoints;

	// Use this for initialization
	void Start ()
	{
		itemPoints = transform.GetComponentsInChildren<ItemPoint>();
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}

	public void PlacePlayer(int index)
	{
		Transform player = Game.instance.currentPiece;

		player.position = playerPositions[index].position;
	}


}
