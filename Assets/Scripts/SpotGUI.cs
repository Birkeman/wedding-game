﻿using UnityEngine;
using System.Collections;

public class SpotGUI : MonoBehaviour
{
	public SpotWidget spotWidgetPrefab;
	public Transform spotWidgets;
	
	// Use this for initialization
	void Start ()
	{
		ItemPoint[] itemPoints = FindObjectsOfType<ItemPoint>();

		RectTransform rectTransform = GetComponent<RectTransform>();

		for(int i = 0; i < itemPoints.Length; i++)
		{
			SpotWidget widget = Instantiate<SpotWidget>(spotWidgetPrefab);
			widget.transform.SetParent(spotWidgets, false);

			widget.itemPoint = itemPoints[i];
			widget.spotTransform = itemPoints[i].transform;
			widget.canvasRect = rectTransform;

			itemPoints[i].spotWidget = widget;

		}

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
