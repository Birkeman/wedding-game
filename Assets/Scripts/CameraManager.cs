﻿using UnityEngine;
using System.Collections;

public class CameraManager : MonoBehaviour
{
	Vector3 offset;

	public Transform[] rooms;

	public int currentRoom = 0;

	public float moveTime = 1;

	Vector3 velocity;

	public GameObject leftArrow;
	public GameObject rightArrow;

	public static CameraManager instance;

	void Awake()
	{
		instance = this;
	}
	
	// Use this for initialization
	void Start ()
	{
		offset = transform.position - rooms[currentRoom].position;

		UpdateArrows();
    }
	
	void Update ()
	{
		transform.position = Vector3.SmoothDamp(transform.position, rooms[currentRoom].position + offset, ref velocity, moveTime);
	}

	public void Move(int direction)
	{
		currentRoom += direction;

		UpdateArrows();
	}

	public void SetRoom(int room)
	{
		currentRoom = room;
		UpdateArrows();
	}

	void UpdateArrows()
	{
		if (currentRoom == 0)
			leftArrow.SetActive(false);
		else
			leftArrow.SetActive(true);

		if (currentRoom >= rooms.Length - 1)
			rightArrow.SetActive(false);
		else
			rightArrow.SetActive(true);
    }
}
