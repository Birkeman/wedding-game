﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Game : MonoBehaviour
{

	public int currentPlayer = 0;
	public int[] playerThemes;
	public string[] playerNames;
	public Transform[] playerPieces;
	
	ItemPoint[] itemPoints;

	public Room[] rooms;

	public static Game instance;

	public Text turnText;
	public Text notificationText;

	public Room currentRoom
	{
		get
		{
			return rooms[CameraManager.instance.currentRoom];
		}
	}

	public Transform currentPiece
	{
		get
		{
			return playerPieces[currentPlayer];
		}
	}

	void Awake()
	{
		instance = this;
	}

	void Update()
	{
		turnText.text = playerNames[currentPlayer] + "'s turn";

		if(Input.GetKeyDown(KeyCode.P))
        {
			if (!running)
				StartCoroutine(ChangeItemsAnim());
			else
				StopCoroutine(ChangeItemsAnim());
		}

	}

	bool running = false;
	public void ProgressTurn()
	{
		currentPlayer++;

		if (currentPlayer >= playerNames.Length)
			currentPlayer = 0;
	}

	public void StartMove()
	{
		NotificationMessage.instance.Show("Choose a room to move to", FinishMove);
	}
	
	public void FinishMove()
	{
		currentRoom.PlacePlayer(currentPlayer);
	}
	
	IEnumerator ChangeItemsAnim()
	{
		for (int i = 0; i < itemPoints.Length; i++)
		{
			ItemPoint point = itemPoints[i];
			int index = Random.Range(0, 3);
			point.ChangeTheme(index);
		}

		while (true)
		{
			for(int i = 0; i < itemPoints.Length; i++)
			{
				ItemPoint point = itemPoints[i];
				int index = point.currentThemeIndex;
				index++;
				if (index > 2)
					index = 0;

				point.ChangeTheme(index);
			}
			
			yield return new WaitForSeconds(1);
		}

	}

	void Start()
	{
		itemPoints = FindObjectsOfType<ItemPoint>();
	}
	
	public void UpdateScoreboard()
	{
		print("Todo");
	}

}
