﻿using UnityEngine;
using System.Collections;

public class Audio : MonoBehaviour
{
	public AudioClip buttonClickedClip;
	public AudioClip itemPlacedClip;
	public AudioClip itemDestroyedClip;

	AudioSource audioSource;

	public static Audio instance;
	
	void Awake()
	{
		instance = this;
	}

	// Use this for initialization
	void Start ()
	{
		audioSource = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	public void PlayButtonClickedSound ()
	{
		audioSource.PlayOneShot(buttonClickedClip);
	}

	public void ItemDestroyed()
	{
		audioSource.PlayOneShot(itemDestroyedClip);
	}

	public void ItemPlacedSound()
	{
		audioSource.PlayOneShot(itemPlacedClip);
	}

}
