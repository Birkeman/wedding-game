﻿using UnityEngine;
using System.Collections;

public class NotificationMessage : MonoBehaviour
{
	public UnityEngine.UI.Text text;
	public GameObject button;

	public static NotificationMessage instance;

	void Awake()
	{
		instance = this;
//		button.SetActive(false);
	//	text.gameObject.SetActive(false);
		gameObject.SetActive(false);
    }
	
	System.Action onDone;

	public void Show(string message, System.Action onDone)
	{
		text.text = message;
		this.onDone = onDone;

//		text.gameObject.SetActive(true);
	//	button.gameObject.SetActive(true);
		gameObject.SetActive(true);

	}

	public void ButtonPressed()
	{
	//	text.gameObject.SetActive(false);
		//button.gameObject.SetActive(false);
		gameObject.SetActive(false);

		onDone();
	}


}
