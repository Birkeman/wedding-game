﻿using UnityEngine;
using System.Collections;

public class ItemPoint : MonoBehaviour
{
	public GameObject[] themes;

	public int pointValue = 1;
	public int cost = 2;

	int startCost;

	public SpotWidget spotWidget;

	public int currentThemeIndex
	{
		get;
		private set;
	}

	public Color[] circleColors;

	SpriteRenderer circle;

	void Start()
	{
		currentThemeIndex = -1;
		startCost = cost;

		circle = transform.Find("Circle").GetComponent<SpriteRenderer>();
    }
	
	public void ChangeTheme(int index)
	{
		if (currentThemeIndex >= 0)
			themes[currentThemeIndex].SetActive(false);

		currentThemeIndex = index;

		if (index >= 0)
		{
			themes[currentThemeIndex].SetActive(true);
			circle.color = circleColors[currentThemeIndex];
			cost += startCost;
			Audio.instance.ItemPlacedSound();
		}
		else
		{
			cost = startCost;
			circle.color = Color.white;
		}

		Game.instance.UpdateScoreboard();
    }

	public void OnMouseUp()
	{
		spotWidget.Show();
	}

}
