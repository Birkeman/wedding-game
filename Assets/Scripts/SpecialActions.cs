﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpecialActions : MonoBehaviour
{
	public GameObject actionsPanel;

	public static SpecialActions instance;
	public Transform explosion;

	void Awake()
	{
		instance = this;
		ClosePanel();
    }
	
	public void OpenPanel()
	{
		actionsPanel.SetActive(true);
	}

	public void ClosePanel()
	{
		actionsPanel.SetActive(false);	
	}
	
	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
	
	public void DrunkenUncle(int room)
	{
		StartCoroutine(DrunkenUncleAnim(room));
	}
	
	IEnumerator DrunkenUncleAnim(int room)
	{
		CameraManager.instance.SetRoom(room);
		ClosePanel();
		yield return new WaitForSeconds(0.6f);

		List<ItemPoint> occupiedPoints = new List<ItemPoint>();

		foreach(ItemPoint point in Game.instance.currentRoom.itemPoints)
		{
			if (point.currentThemeIndex >= 0)
				occupiedPoints.Add(point);
		}

		int randomPoint = Random.Range(0, occupiedPoints.Count);

		occupiedPoints[randomPoint].ChangeTheme(-1);
		Vector3 position = occupiedPoints[randomPoint].transform.position;
		Instantiate(explosion, position, Quaternion.identity);
		Audio.instance.ItemDestroyed();
    }
}
