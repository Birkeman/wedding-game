﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class CameraObliquer : MonoBehaviour {

	[SerializeField] float horizObl;
	[SerializeField] float vertObl;
	
	void Update()
	{
		SetObliqueness();
	}

	void SetObliqueness()
	{
		Camera.main.ResetProjectionMatrix();
		Matrix4x4 mat = Camera.main.projectionMatrix;
		mat[0, 2] = horizObl;
		mat[1, 2] = vertObl;
		Camera.main.projectionMatrix = mat;
	}
}
