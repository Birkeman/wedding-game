﻿using UnityEngine;
using System.Collections;

public class SpotWidget : MonoBehaviour {

	public Transform spotTransform;
	public ItemPoint itemPoint;
	public RectTransform canvasRect;
	
	public UnityEngine.UI.Text headline;
	public UnityEngine.UI.Text pointsText;
	public UnityEngine.UI.Text costText;

	public UnityEngine.UI.Text legendText;
	public GameObject legend;

	public GameObject background;

	RectTransform rectTransform;
	
	// Use this for initialization
	void Start ()
	{
		Hide();
		rectTransform = GetComponent<RectTransform>();

		if (spotTransform == null)
		{
			legend.SetActive(false);
			return;
		}
		headline.text = "Select item for " + spotTransform.name.Replace("Spot", "");
		legendText.text = spotTransform.name.Replace("Spot", "");

		pointsText.text = itemPoint.pointValue + " Pts";
    }
	
	void Update()
	{
		if (spotTransform == null)
			return;

		//Vector2 pos = spotTransform.position;  // get the game object position
		//Vector2 viewportPoint = Camera.main.WorldToViewportPoint(pos);  //convert game object position to VievportPoint

		//// set MIN and MAX Anchor values(positions) to the same position (ViewportPoint)
		//rectTransform.anchorMin = viewportPoint;
		//rectTransform.anchorMax = viewportPoint;


		//this is your object that you want to have the UI element hovering over
		//GameObject WorldObject;

		//this is the ui element
		//RectTransform UI_Element;

		//first you need the RectTransform component of your canvas
		//RectTransform CanvasRect = Canvas.GetComponent<RectTransform>();

		//then you calculate the position of the UI element
		//0,0 for the canvas is at the center of the screen, whereas WorldToViewPortPoint treats the lower left corner as 0,0. Because of this, you need to subtract the height / width of the canvas * 0.5 to get the correct position.

		Vector2 ViewportPosition = Camera.main.WorldToViewportPoint(spotTransform.position);
		Vector2 WorldObject_ScreenPosition = new Vector2(
		((ViewportPosition.x * canvasRect.sizeDelta.x) - (canvasRect.sizeDelta.x * 0.5f)),
		((ViewportPosition.y * canvasRect.sizeDelta.y) - (canvasRect.sizeDelta.y * 0.5f)));

		//now you can set the position of the ui element
		rectTransform.anchoredPosition = WorldObject_ScreenPosition;

		//		rectTransform.anchoredPosition = screenPoint;// + canvasRect.sizeDelta / 2f;

		costText.text = itemPoint.cost + " $";
	}
	
	public void Show()
	{
		background.SetActive(true);
		legend.SetActive(false);
    }

	public void Hide()
	{
		background.SetActive(false);
		legend.SetActive(true);
    }

	public void ItemSelected(int index)
	{
		itemPoint.ChangeTheme(index);
		Hide();
	}
}
